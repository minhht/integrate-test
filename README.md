Integrate Test

## Requirements

- PHP 7.1
- Composer (in case there is no Composer install, can use composer.phar file including in the project)

## Usage

- Install with Composer (optional, already installing)

```
php composer.phar install
```
- Run migration from Doctrine ORM tool

```
./bin/doctrine orm:schema-tool:create
```

- Run with built-in PHP server from root of project, you can choose different port to run on

```
php -S localhost:9898 -t public public/index.php
```

- The API uses basic authentication with username `test` and password `integrate`

## How to run tests

```
./bin/phpunit
```
