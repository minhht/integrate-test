<?php
namespace Tests\Api\Lead;

use Application\Api\SlimApplication;
use Tests\Api\APITest;
use FR3D\SwaggerAssertions\PhpUnit\AssertsTrait;

class LeadPUTTest extends APITest
{
    use AssertsTrait;

    protected function setUp()
    {
        $container = SlimApplication::container();
        $em = $container->get('em');
        $em->getConnection()->exec('
INSERT INTO leads(first_name, last_name, email, company, post_code, has_accepted, date_created) VALUES ("test 1", "lead 1", "lead1@example.com", "leadtest", "W11AB", 1, CURRENT_TIMESTAMP);
');
    }

    /*
     * @test
     */
    public function testUpdating()
    {
        $response = $this->dispatch('/leads/1', 'PUT', [
           'first_name' => 'test',
           'last_name' => 'lead',
           'email' => 'test@example.com',
           'company' => 'testlead',
           'post_code' => 'W11AB',
           'has_accepted' => false,
        ]);
        $data = json_decode($response->getBody());
        self::assertResponseBodyMatch(
            $data,
            $this->schema,
            '/leads/1',
            'put',
            200
        );
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotNull($data->id);
        $this->assertSame('test', $data->first_name);
        $this->assertSame('lead', $data->last_name);
        $this->assertSame('test@example.com', $data->email);
        $this->assertSame('testlead', $data->company);
        $this->assertSame('W11AB', $data->post_code);
        $this->assertFalse($data->has_accepted);
        $this->assertSame((new \DateTime)->format('Y-m-d'), $data->date_created);
    }

    /*
     * @test
     */
    public function testFailedUpdating()
    {
        $response = $this->dispatch('/leads/10', 'PUT', [
           'first_name' => 'test',
           'last_name' => 'lead',
           'email' => 'test@example.com',
           'company' => 'testlead',
           'post_code' => 'W11AB',
           'has_accepted' => false,
        ]);
        $this->assertEquals(404, $response->getStatusCode());

        $response = $this->dispatch('/leads/1', 'PUT', [
           'first_name' => 'test',
           'last_name' => 'lead',
           'email' => 'test',
           'company' => 'testlead',
           'post_code' => 'W11AB',
           'has_accepted' => false,
        ]);
        $this->assertEquals(400, $response->getStatusCode());
    }
}
