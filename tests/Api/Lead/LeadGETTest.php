<?php
namespace Tests\Api\Lead;

use Application\Api\SlimApplication;
use Tests\Api\APITest;
use FR3D\SwaggerAssertions\PhpUnit\AssertsTrait;

class LeadGETTest extends APITest
{
    use AssertsTrait;

    protected function setUp()
    {
        $container = SlimApplication::container();
        $em = $container->get('em');
        $em->getConnection()->exec('
INSERT INTO leads(first_name, last_name, email, company, post_code, has_accepted, date_created) VALUES ("test 1", "lead 1", "lead1@example.com", "leadtest", "W11AB", 1, CURRENT_TIMESTAMP);
INSERT INTO leads(first_name, last_name, email, company, post_code, has_accepted, date_created) VALUES ("test 2", "lead 2", "lead2@example.com", "leadtest", "W11AB", 1, CURRENT_TIMESTAMP);
INSERT INTO leads(first_name, last_name, email, company, post_code, has_accepted, date_created) VALUES ("test 3", "lead 3", "lead3@example.com", "leadtest", "W11AB", 1, CURRENT_TIMESTAMP);
INSERT INTO leads(first_name, last_name, email, company, post_code, has_accepted, date_created) VALUES ("test 4", "lead 4", "lead4@example.com", "leadtest", "W11AB", 1, CURRENT_TIMESTAMP);
INSERT INTO leads(first_name, last_name, email, company, post_code, has_accepted, date_created) VALUES ("test 5", "lead 5", "lead5@example.com", "leadtest", "W11AB", 1, CURRENT_TIMESTAMP);
INSERT INTO leads(first_name, last_name, email, company, post_code, has_accepted, date_created) VALUES ("test 6", "lead 6", "lead6@example.com", "leadtest", "W11AB", 1, CURRENT_TIMESTAMP);
');
    }

    /*
     * @test
     */
    public function testListing()
    {
        $response = $this->dispatch('/leads');
        $data = json_decode($response->getBody());
        self::assertResponseBodyMatch(
            $data,
            $this->schema,
            '/leads',
            'get',
            200
        );
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertSame(6, count($data));
    }

    /*
     * @test
     */
    public function testGettingAnItem()
    {
        $response = $this->dispatch('/leads/2');
        $data = json_decode($response->getBody());
        self::assertResponseBodyMatch(
            $data,
            $this->schema,
            '/leads/2',
            'get',
            200
        );
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertSame(2, $data->id);
        $this->assertSame('test 2', $data->first_name);
        $this->assertSame('lead 2', $data->last_name);
        $this->assertSame('lead2@example.com', $data->email);
        $this->assertSame('leadtest', $data->company);
        $this->assertSame('W11AB', $data->post_code);
        $this->assertTrue($data->has_accepted);
        $this->assertSame((new \DateTime)->format('Y-m-d'), $data->date_created);
    }

    /*
     * @test
     */
    public function testGettingNonExistingItem()
    {
        $response = $this->dispatch('/leads/20');
        $this->assertEquals(404, $response->getStatusCode());
    }

    /*
     * @test
     */
    public function testFilteringByEmail()
    {
        $container = SlimApplication::container();
        $em = $container->get('em');
        $em->getConnection()->exec('
INSERT INTO leads(first_name, last_name, email, company, post_code, has_accepted, date_created) VALUES ("test 1", "lead 1", "leadtest@example.com", "leadtest", "W11AB", 1, CURRENT_TIMESTAMP);
');
        $response = $this->dispatch('/leads?email=leadtest@example.com');
        $data = json_decode($response->getBody());
        self::assertResponseBodyMatch(
            $data,
            $this->schema,
            '/leads',
            'get',
            200
        );
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertSame(1, count($data));
    }
}
