<?php

namespace Tests\Api\Lead;

use Application\Api\SlimApplication;
use Tests\Api\APITest;
use FR3D\SwaggerAssertions\PhpUnit\AssertsTrait;

class LeadDELETETest extends APITest
{
    use AssertsTrait;

    protected function setUp()
    {
        $container = SlimApplication::container();
        $em = $container->get('em');
        $em->getConnection()->exec('
INSERT INTO leads(first_name, last_name, email, company, post_code, has_accepted, date_created) VALUES ("test 1", "lead 1", "lead1@example.com", "leadtest", "W11AB", 1, CURRENT_TIMESTAMP);
');
    }

    /*
     * @test
     */
    public function testDeleting()
    {
        $response = $this->dispatch('/leads/1', 'DELETE');
        self::assertResponseBodyMatch(
            $data,
            $this->schema,
            '/leads/1',
            'delete',
            204
        );
        $this->assertEquals(204, $response->getStatusCode());
    }

    /*
     * @test
     */
    public function testDeletingWrongEntity()
    {
        $response = $this->dispatch('/leads/3', 'DELETE');
        $this->assertEquals(404, $response->getStatusCode());
    }
}
