<?php
namespace Tests\Api\Lead;

use Application\Api\SlimApplication;
use Tests\Api\APITest;
use FR3D\SwaggerAssertions\PhpUnit\AssertsTrait;

class LeadPOSTTest extends APITest
{
    use AssertsTrait;

    /*
     * @test
     */
    public function testCreating()
    {
        $response = $this->dispatch('/leads', 'POST', [
           'first_name' => 'test',
           'last_name' => 'lead',
           'email' => 'test@example.com',
           'company' => 'testlead',
           'post_code' => 'W11AB',
           'has_accepted' => true,
        ]);
        $data = json_decode($response->getBody());
        self::assertResponseBodyMatch(
            $data,
            $this->schema,
            '/leads',
            'post',
            201
        );
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertNotNull($data->id);
        $this->assertSame('test', $data->first_name);
        $this->assertSame('lead', $data->last_name);
        $this->assertSame('test@example.com', $data->email);
        $this->assertSame('testlead', $data->company);
        $this->assertSame('W11AB', $data->post_code);
        $this->assertTrue($data->has_accepted);
        $this->assertSame((new \DateTime)->format('Y-m-d'), $data->date_created);
    }

    /*
     * @test
     */
    public function testFailedCreating()
    {
        $response = $this->dispatch('/leads', 'POST', [
           'first_name' => 'test',
           'last_name' => 'lead',
           'company' => 'testlead',
           'post_code' => 'W11AB',
           'has_accepted' => true,
        ]);
        $this->assertEquals(400, $response->getStatusCode());
    }
}
