<?php
namespace Tests\Api;

use PHPUnit\Framework\TestCase;
use Application\Api\SlimApplication;
use Symfony\Component\Process\Process;
use Psr\Http\Message\ResponseInterface;
use FR3D\SwaggerAssertions\SchemaManager;
use GuzzleHttp\Client;

abstract class APITest extends TestCase
{
    const ENVIRONMENT = "test";
    const HOST = "127.0.0.1";
    const PORT = 9876;

    protected static $process;

    protected $schema;

    public static function setUpBeforeClass()
    {
        // create db
        $dbPath = __DIR__.'/../../src/Integration/Doctrine/db.sqlite';
        if (!file_exists($dbPath)) {
            fopen($dbPath, 'w') or die('Cannot open file:  ' . $dbPath);
        }
        $queryPath = __DIR__.'/../../src/Integration/Doctrine/query.sql';
        if (!file_exists($queryPath)) {
            die('Cannot open file:  ' . $dbPath);
        }
        $handle = fopen($queryPath, 'r');
        $sql = fread($handle,filesize($queryPath));
        $container = SlimApplication::container();
        $em = $container->get('em');
        $em->getConnection()->exec($sql);
        // create command line
        $command = sprintf(
          'ENVIRONMENT=%s php -S %s:%d -t %s %s',
          self::ENVIRONMENT,
          self::HOST,
          self::PORT,
          realpath(__DIR__.'/../../public'),
          realpath(__DIR__.'/../../public/index.php')
        );
        // create process to run command
        self::$process = new Process($command);
        self::$process->disableOutput();
        // start server
        self::$process->start();
        // provide time to initialise server
        usleep(100000);
    }

    public static function tearDownAfterClass()
    {
        $dbPath = __DIR__.'/../../src/Integration/Doctrine/db.sqlite';
        $handle = fopen($dbPath, 'w') or die('Cannot open file:  ' . $dbPath);
        fwrite($handle, '');
        self::$process->stop();
    }

    protected function dispatch(string $path, string $method = 'GET', array $data = []): ResponseInterface
    {
        $this->schema = new SchemaManager(json_decode(file_get_contents(
            __DIR__ . '/../../documentation/api.json'
        )));
        $params = [];
        if ($data && $method === 'POST') {
            $params['form_params'] = $data;
        }
        if ($data && $method === 'PUT') {
            $params['json'] = $data;
        }

        $client = new Client([
            'base_uri' => 'http://' . self::HOST . ':' . self::PORT,
            'auth' => ['test', 'integrate'],
            'http_errors' => false,
        ]);
        return $client->request($method, $path, $params);
    }
}
