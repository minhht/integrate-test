<?php

namespace Tests\Unit\Application\Authentication;

use Application\Authentication\BasicAuthentication;
use Psr\Http\Message\ServerRequestInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Handler\MockHandler;
use PHPUnit\Framework\TestCase;

class BasicAuthenticationTest extends TestCase
{
    /*
     * @test
     */
    public function testValid()
    {
        $mock = new MockHandler([new Response()]);
        $client = new Client(['handler' => $mock]);
        $client->get('localhost', ['auth' => ['test', 'integrate']]);
        $request = $mock->getLastRequest();
        $service = new BasicAuthentication();
        $this->assertTrue($service->isValid($request));
    }

    /*
     * @test
     */
    public function testInvalid()
    {
        $service = new BasicAuthentication();

        $mock = new MockHandler([new Response()]);
        $client = new Client(['handler' => $mock]);
        $client->get('localhost');
        $request = $mock->getLastRequest();
        $this->assertFalse($service->isValid($request));

        $mock = new MockHandler([new Response()]);
        $client = new Client(['handler' => $mock]);
        $client->get('localhost', ['auth' => ['test', 'test']]);
        $request = $mock->getLastRequest();
        $this->assertFalse($service->isValid($request));
    }
}
