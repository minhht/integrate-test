<?php
require_once(__DIR__ . '/bootstrap/bootstrap.php');

$container = Application\Api\SlimApplication::container();
$em = $container->get('em');
$helperSet = new Symfony\Component\Console\Helper\HelperSet([
    'db' => new Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($em->getConnection()),
    'em' => new Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em),
]);
return $helperSet;
