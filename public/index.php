<?php

if (PHP_SAPI == 'cli-server') {
    // serve static file for built-in PHP server
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require_once(__DIR__ . '/../bootstrap/bootstrap.php');

$app = Application\Api\SlimApplication::create();

$app->run();
