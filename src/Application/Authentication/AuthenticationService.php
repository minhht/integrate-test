<?php

namespace Application\Authentication;

use Psr\Http\Message\RequestInterface as Request;

interface AuthenticationService
{
    /**
     * Check if the request is authenticated
     *
     * @param RequestInterface $request
     *
     * @return bool
     */
    public function isValid(Request $request): bool;
}
