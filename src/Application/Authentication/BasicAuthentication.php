<?php

namespace Application\Authentication;

use Application\Authentication\AuthenticationService;
use Psr\Http\Message\RequestInterface as Request;

class BasicAuthentication implements AuthenticationService
{
    private const USER_NAME = 'test';

    private const PASSWORD = 'integrate';

    /**
     * Check if the request is authenticated
     *
     * @param  ServerRequestInterface $request
     *
     * @return bool
     */
    public function isValid(Request $request): bool
    {
        // Basic dGVzdDppbnRlZ3JhdGU=
        $authHeaders = explode(' ', $request->getHeaderLine('Authorization'));
        if (count($authHeaders) !== 2 || $authHeaders[0] != 'Basic') {
            return false;
        }
        $credential = explode(':', base64_decode($authHeaders[1]));
        if (count($credential) !== 2 ||
            $credential[0] !== self::USER_NAME ||
            $credential[1] !== self::PASSWORD
        ) {
            return false;
        }
        return true;
    }
}
