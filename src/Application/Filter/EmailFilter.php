<?php

namespace Application\Filter;

use Integrate\LeadList;
use Integration\Doctrine\DoctrineLeadList;

class EmailFilter
{

    /** @var string */
    private $email;

    /**
     * Filter for email
     *
     * @param string $email
     */
    public function __construct(string $email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
           throw new \Exception('Invalid email');
        }
        $this->email = $email;
    }

    /**
     * Apply filter for each implementation of LeadList
     *
     * @param LeadList $list
     */
    public function apply(LeadList $list)
    {
        if ($list instanceof DoctrineLeadList) {
            $list->setEmailFilter($this->email);
            return;
        }
        throw new \Exception('Has not supported this list implementation yet' . get_class($list));
    }
}
