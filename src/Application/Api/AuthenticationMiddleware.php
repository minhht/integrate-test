<?php

namespace Application\Api;

use Slim\Factory\Psr17\SlimPsr17Factory;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;

class AuthenticationMiddleware
{
    /** @var ContainerInterface */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
       $this->container = $container;
    }

    /**
     * Handle authentication
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function __invoke(Request $request, RequestHandler $handler): Response
    {
        $authService = $this->container->get('authService');
        if (!$authService->isValid($request)) {
            $factory = SlimPsr17Factory::getResponseFactory();
            return $factory->createResponse(401);
        }
        $response = $handler->handle($request);
        return $response;
    }
}
