<?php

namespace Application\Api;

use Application\Api\LeadController;
use Application\Api\AuthenticationMiddleware;
use Application\Authentication\BasicAuthentication;
use DI\Container;
use DI\ContainerBuilder;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Integration\Doctrine\DoctrineLeadBuilder;
use Integration\Doctrine\DoctrineLeadList;
use Integration\Doctrine\DoctrineLeadStorage;
use Slim\Factory\AppFactory;
use Slim\App;
use Psr\Container\ContainerInterface;

class SlimApplication
{
    /**
     * Create container
     *
     * @return ContainerInterface
     */
    public static function container(): ContainerInterface
    {
        // php-di container
        $builder = new ContainerBuilder();
        $builder->addDefinitions([
            'em' => \DI\factory(function(ContainerInterface $c) {
                $config = Setup::createAnnotationMetadataConfiguration(
                    [__DIR__ . '/../../Integration/Doctrine'],
                    true
                );
                $connection = [
                    'driver' => 'pdo_sqlite',
                    'path' => __DIR__ . '/../../Integration/Doctrine/db.sqlite',
                ];
                return EntityManager::create($connection, $config);
            }),
            'authService' => \DI\factory(function(ContainerInterface $c) {
                return new BasicAuthentication;
            }),
            'builder' => \DI\factory(function(ContainerInterface $c) {
                return new DoctrineLeadBuilder($c->get('em'));
            }),
            'list' => \DI\factory(function(ContainerInterface $c) {
                return new DoctrineLeadList($c->get('em'));
            }),
            'storage' => \DI\factory(function(ContainerInterface $c) {
                return new DoctrineLeadStorage($c->get('em'));
            }),
        ]);

        return $builder->build();
    }

    /**
     * Create Slim application
     *
     * @return App
     */
    public static function create(): App
    {
        // slim app
        AppFactory::setContainer(self::container());
        $app = AppFactory::create();
        // middleware
        $app->add(new AuthenticationMiddleware(self::container()));
        // routes
        $app->get('/leads', 'Application\Api\LeadController:list');
        $app->get('/leads/{id:[0-9]+}', 'Application\Api\LeadController:get');
        $app->post('/leads', 'Application\Api\LeadController:create');
        $app->put('/leads/{id:[0-9]+}', 'Application\Api\LeadController:update');
        $app->delete('/leads/{id:[0-9]+}', 'Application\Api\LeadController:delete');

        $app->addErrorMiddleware(true, true, true);
        return $app;
    }
}
