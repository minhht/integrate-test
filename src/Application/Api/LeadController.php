<?php

namespace Application\Api;

use Application\Filter\EmailFilter;
use Integrate\Lead;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class LeadController
{
    /** @var ContainerInterface */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
       $this->container = $container;
    }

    /**
     * Handle getting list request
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     */
    public function list(Request $request, Response $response): Response
    {
       $list = $this->container->get('list');
       $query = $request->getQueryParams();
       if (key_exists('email', $query)) {
           $filter = new EmailFilter($query['email']);
           $filter->apply($list);
       }

       $leads = $list->get();
       $payload = array_map(function(Lead $lead) {
           return [
               'id' => $lead->getId(),
               'first_name' => $lead->getFirstName(),
               'last_name' => $lead->getLastName(),
               'email' => $lead->getEmail(),
               'company' => $lead->getCompany(),
               'post_code' => $lead->getPostCode(),
               'has_accepted' => $lead->hasAccepted(),
               'date_created' => $lead->getDateCreated()->format('Y-m-d'),
           ];
       }, $leads);

       $response->getBody()->write(json_encode($payload));
       return $response
           ->withHeader('Content-type', 'application/json')
           ->withStatus(200);
    }

    /**
     * Handle getting an item request
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param array $args
     *
     * @return ResponseInterface
     */
    public function get(Request $request, Response $response, array $args)
    {
       $list = $this->container->get('list');
       try {
           $lead = $list->getById((int)$args['id']);
       } catch (\Exception $e) {
           return $response
               ->withHeader('Content-type', 'application/json')
               ->withStatus(404);
       }
       $response->getBody()->write(json_encode([
           'id' => $lead->getId(),
           'first_name' => $lead->getFirstName(),
           'last_name' => $lead->getLastName(),
           'email' => $lead->getEmail(),
           'company' => $lead->getCompany(),
           'post_code' => $lead->getPostCode(),
           'has_accepted' => $lead->hasAccepted(),
           'date_created' => $lead->getDateCreated()->format('Y-m-d'),
       ]));
       return $response
           ->withHeader('Content-type', 'application/json')
           ->withStatus(200);
    }

    /**
     * Handle creating request
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     */
    public function create(Request $request, Response $response)
    {
       $builder = $this->container->get('builder');
       $storage = $this->container->get('storage');
       $statusCode = 201;

       try {
           $payload = $request->getParsedBody() ?? [];

           if (key_exists('first_name', $payload)) {
               $builder->setFirstName(filter_var($payload['first_name'], FILTER_SANITIZE_STRING));
           }
           if (key_exists('last_name', $payload)) {
               $builder->setLastName(filter_var($payload['last_name'], FILTER_SANITIZE_STRING));
           }
           if (key_exists('email', $payload) && filter_var($payload['email'], FILTER_VALIDATE_EMAIL)) {
               $builder->setEmail($payload['email']);
           }
           if (key_exists('company', $payload)) {
               $builder->setCompany(filter_var($payload['company'], FILTER_SANITIZE_STRING));
           }
           if (key_exists('post_code', $payload)) {
               $builder->setPostCode(filter_var($payload['post_code'], FILTER_SANITIZE_STRING));
           }
           if (key_exists('has_accepted', $payload)) {
               $builder->accept((bool)$payload['has_accepted']);
           }
           $lead = $builder->build();
           $storage->save($lead);

           $response->getBody()->write(json_encode([
               'id' => $lead->getId(),
               'first_name' => $lead->getFirstName(),
               'last_name' => $lead->getLastName(),
               'email' => $lead->getEmail(),
               'company' => $lead->getCompany(),
               'post_code' => $lead->getPostCode(),
               'has_accepted' => $lead->hasAccepted(),
               'date_created' => $lead->getDateCreated()->format('Y-m-d'),
           ]));
       } catch (\Exception $e) {
           $statusCode = 400;
       }
       return $response
           ->withHeader('Content-type', 'application/json')
           ->withStatus($statusCode);
    }

    /**
     * Handle updating request
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param array $args
     *
     * @return ResponseInterface
     */
    public function update(Request $request, Response $response, array $args)
    {
       $list = $this->container->get('list');
       $storage = $this->container->get('storage');
       $statusCode = 200;

       try {
           $lead = $list->getById((int)$args['id']);
       } catch (\Exception $e) {
           return $response
               ->withHeader('Content-type', 'application/json')
               ->withStatus(404);
       }
       try {
           $payload = json_decode($request->getBody()->getContents(), true) ?? [];

           if (key_exists('first_name', $payload)) {
               $lead->setFirstName(filter_var($payload['first_name'], FILTER_SANITIZE_STRING));
           }
           if (key_exists('last_name', $payload)) {
               $lead->setLastName(filter_var($payload['last_name'], FILTER_SANITIZE_STRING));
           }
           if (key_exists('email', $payload)) {
               if (!filter_var($payload['email'], FILTER_VALIDATE_EMAIL)) {
                   throw new \Exception('Invalid email');
               }
               $lead->setEmail($payload['email']);
           }
           if (key_exists('company', $payload)) {
               $lead->setCompany(filter_var($payload['company'], FILTER_SANITIZE_STRING));
           }
           if (key_exists('post_code', $payload)) {
               $lead->setPostCode(filter_var($payload['post_code'], FILTER_SANITIZE_STRING));
           }
           if (key_exists('has_accepted', $payload)) {
               $lead->accept((bool)$payload['has_accepted']);
           }
           $storage->save($lead);

           $response->getBody()->write(json_encode([
               'id' => $lead->getId(),
               'first_name' => $lead->getFirstName(),
               'last_name' => $lead->getLastName(),
               'email' => $lead->getEmail(),
               'company' => $lead->getCompany(),
               'post_code' => $lead->getPostCode(),
               'has_accepted' => $lead->hasAccepted(),
               'date_created' => $lead->getDateCreated()->format('Y-m-d'),
           ]));
       } catch (\Exception $e) {
           $statusCode = 400;
       }
       return $response
           ->withHeader('Content-type', 'application/json')
           ->withStatus($statusCode);
    }

    /**
     * Handle deleting request
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     */
    public function delete(Request $request, Response $response, array $args)
    {
       $list = $this->container->get('list');
       $storage = $this->container->get('storage');
       $statusCode = 204;

       try {
           $lead = $list->getById((int)$args['id']);
           $storage->remove($lead);
       } catch (\Exception $e) {
           $statusCode = 404;
       }

       return $response
           ->withHeader('Content-type', 'application/json')
           ->withStatus($statusCode);
    }
}
