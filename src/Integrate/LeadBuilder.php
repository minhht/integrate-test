<?php

namespace Integrate;

use Integrate\Lead;

abstract class LeadBuilder
{
    /** @var string */
    protected $firstName;

    /** @var string */
    protected $lastName;

    /** @var string */
    protected $email;

    /** @var string|null */
    protected $company;

    /** @var string|null */
    protected $postCode;

    /** @var bool */
    protected $hasAccepted = true;

    /**
     * Set first name
     *
     * @param string $firstName
     */
    public function setFirstName(string $firstName): LeadBuilder
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * Set last name
     *
     * @param string $lastName
     */
    public function setLastName(string $lastName): LeadBuilder
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail(string $email): LeadBuilder
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Set company
     *
     * @param string $company
     */
    public function setCompany(string $company): LeadBuilder
    {
        $this->company = $company;
        return $this;
    }

    /**
     * Set post code
     *
     * @param string $postCode
     */
    public function setPostCode(string $postCode): LeadBuilder
    {
        $this->postCode = $postCode;
        return $this;
    }

    /**
     * Set acceptance terms
     *
     * @param bool $hasAccepted
     */
    public function accept(bool $hasAccepted): LeadBuilder
    {
        $this->hasAccepted = $hasAccepted;
        return $this;
    }

    /**
     * Create Lead object
     *
     * @return Lead
     */
    public function build(): Lead
    {
        if (!$this->firstName ||
            !$this->lastName ||
            !$this->email) {
            throw new \Exception('Missing value to create Lead');
        }
        return $this->buildFromImplementation();
    }

    /**
     * Create Lead object from implementation of this class
     *
     * @return Lead
     */
    abstract protected function buildFromImplementation(): Lead;
}
