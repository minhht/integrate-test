<?php

namespace Integrate;

interface Lead
{
    /**
     * Get id of object
     *
     * @return int
     */
    public function getId(): int;

    /**
     * Get first name
     *
     * @return string
     */
    public function getFirstName(): string;

    /**
     * Get get last name
     *
     * @return string
     */
    public function getLastName(): string;

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail(): string;

    /**
     * Get postcode
     *
     * @return string|null
     */
    public function getPostCode();

    /**
     * Get company
     *
     * @return string|null
     */
    public function getCompany();

    /**
     * Check if it has accepted terms
     *
     * @return bool
     */
    public function hasAccepted(): bool;

    /**
     * Get date created
     *
     * @return \DateTime
     */
    public function getDateCreated(): \DateTime;

    /**
     * Set first name
     *
     * @param string $firstName
     */
    public function setFirstName(string $firstName);

    /**
     * Set last name
     *
     * @param string $lastName
     */
    public function setLastName(string $lastName);

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail(string $email);

    /**
     * Set company
     *
     * @param string $company
     */
    public function setCompany(string $company);

    /**
     * Set post code
     *
     * @param string $postCode
     */
    public function setPostCode(string $postCode);

    /**
     * Set acceptance terms
     *
     * @param bool $hasAccepted
     */
    public function accept(bool $hasAccepted);
}
