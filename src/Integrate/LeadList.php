<?php

namespace Integrate;

use Integrate\Lead;

interface LeadList
{
    /**
     * Get list of Lead
     *
     * @return array
     */
    public function get(): array;

    /**
     * Get Lead object by id
     *
     * @param int $id
     * @return Lead
     */
    public function getById(int $id): Lead;
}
