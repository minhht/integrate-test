<?php

namespace Integrate;

use Integrate\Lead;

interface LeadStorage
{
    /**
     * Persist Lead object
     *
     * @param Lead $lead
     * @return bool
     */
    public function save(Lead $lead): bool;

    /**
     * Delete Lead object
     *
     * @param Lead $lead
     * @return bool
     */
    public function remove(Lead $lead): bool;
}
