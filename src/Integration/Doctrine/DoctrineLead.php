<?php

namespace Integration\Doctrine;

use Integrate\Lead;
use Integration\Doctrine\DoctrineLeadBuilder;

/**
 * @Entity
 * @Table(name="leads")
 */
class DoctrineLead implements Lead
{
    /**
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @Column(name="first_name", type="string")
     */
    private $firstName;

    /**
     * @Column(name="last_name", type="string")
     */
    private $lastName;

    /**
     * @Column(name="email", type="string")
     */
    private $email;

    /**
     * @Column(name="company", type="string", nullable=true)
     */
    private $company;

    /**
     * @Column(name="post_code", type="string", nullable=true)
     */
    private $postCode;

    /**
     * @Column(name="has_accepted", type="boolean")
     */
    private $hasAccepted;

    /**
     * @Column(name="date_created", type="datetime")
     */
    private $dateCreated;

    /**
     * @param DoctrineLeadBuilder $builder
     */
    public function __construct(DoctrineLeadBuilder $builder)
    {
        $this->firstName = $builder->getFirstName();
        $this->lastName = $builder->getLastName();
        $this->email = $builder->getEmail();
        $this->company = $builder->getCompany();
        $this->postCode = $builder->getPostCode();
        $this->hasAccepted = $builder->hasAccepted();
        $this->dateCreated = new \DateTime;
    }

    /**
     * Get id of object
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Get first name
     *
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * Get get last name
     *
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Get postcode
     *
     * @return string|null
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * Get company
     *
     * @return string|null
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Check if it has accepted terms
     *
     * @return bool
     */
    public function hasAccepted(): bool
    {
        return $this->hasAccepted;
    }

    /**
     * Get date created
     *
     * @return \DateTime
     */
    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

    /**
     * Set first name
     *
     * @param string $firstName
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * Set last name
     *
     * @param string $lastName
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * Set company
     *
     * @param string $company
     */
    public function setCompany(string $company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * Set post code
     *
     * @param string $postCode
     */
    public function setPostCode(string $postCode)
    {
        $this->postCode = $postCode;
        return $this;
    }

    /**
     * Set acceptance terms
     *
     * @param bool $hasAccepted
     */
    public function accept(bool $hasAccepted)
    {
        $this->hasAccepted = $hasAccepted;
    }
}
