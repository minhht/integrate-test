<?php
namespace Integration\Doctrine;

use Integrate\Lead;
use Integrate\LeadBuilder;
use Integration\Doctrine\DoctrineLead;
use Doctrine\ORM\EntityManager;

class DoctrineLeadBuilder extends LeadBuilder
{
    /** @var EntityManager */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Get first name
     *
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * Get get last name
     *
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Get postcode
     *
     * @return string|null
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * Get company
     *
     * @return string|null
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Check if it has accepted terms
     *
     * @return bool
     */
    public function hasAccepted(): bool
    {
        return $this->hasAccepted;
    }

    /**
     * Create Lead object from implementation of this class
     *
     * @return Lead
     */
    protected function buildFromImplementation(): Lead
    {
        $lead = new DoctrineLead($this);
        $this->entityManager->persist($lead);
        $this->entityManager->flush();
        return $lead;
    }
}
