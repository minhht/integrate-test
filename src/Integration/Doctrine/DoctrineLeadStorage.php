<?php
namespace Integration\Doctrine;

use Integrate\Lead;
use Integrate\LeadStorage;
use Doctrine\ORM\EntityManager;

class DoctrineLeadStorage implements LeadStorage
{
    /** @var EntityManager */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Persist Lead object
     *
     * @param Lead $lead
     * @return bool
     */
    public function save(Lead $lead): bool
    {
        try {
            $this->entityManager->persist($lead);
            $this->entityManager->flush();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Delete Lead object
     *
     * @param Lead $lead
     * @return bool
     */
    public function remove(Lead $lead): bool
    {
        try {
            $this->entityManager->remove($lead);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
