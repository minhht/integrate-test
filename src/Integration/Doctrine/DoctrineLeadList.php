<?php
namespace Integration\Doctrine;

use Integrate\Lead;
use Integrate\LeadList;
use Integration\Doctrine\DoctrineLead;
use Doctrine\ORM\EntityManager;

class DoctrineLeadList implements LeadList
{
    /** @var EntityManager */
    private $entityManager;

    /** @var string|null */
    private $email;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Get list of Lead
     *
     * @return array
     */
    public function get(): array
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder
            ->select('l')
            ->from(DoctrineLead::class, 'l');
        if ($this->email) {
            $queryBuilder
                ->where('l.email = :email')
                ->setParameter('email', $this->email);
        }
        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * Get Lead object by id
     *
     * @param int $id
     * @return Lead
     */
    public function getById(int $id): Lead
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder
            ->select('l')
            ->from(DoctrineLead::class, 'l');
        $lead = $this->entityManager->find(DoctrineLead::class, $id);
        if (!$lead) {
            throw new \Exception('Could not found Lead object with id : ' . $id);
        }
        return $lead;
    }

    /**
     * Set email filter
     *
     * @param string $email
     */
    public function setEmailFilter(string $email)
    {
        $this->email = $email;
    }
}
